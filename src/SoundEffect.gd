extends AudioStreamPlayer
class_name SoundEffect


export var signal_waves_per_second: float = 440.0
export var samples_per_second: int = 44100 # less samples to mix, GDScript is not super fast for this
export var attack_seconds: float = 0.05
export var fade_seconds: float = 0.05
export var duration_seconds: float = 2


var _phase: float = 0.0
var _playback: AudioStreamPlayback = null # object that does the actual playback
var _seconds_since_start: float = 0.0


var _time_since_start_seconds: float = 0.0


func _ready():
#	$player.stream.mix_rate=samples_per_second # setting samples_per_second is only possible before playing
	self.stream.mix_rate = samples_per_second # setting samples_per_second is only possible before playing
#	playback = $player.get_stream_playback()
	self._playback = get_stream_playback()
	self._fill_buffer() # prefill, do before play to avoid delay 
#	$player.play() # start
	self.play()


func _fill_buffer():
	var wave_per_sample: float = self.signal_waves_per_second / self.samples_per_second
	var current_sample: int = 0
	var amplitude: float
	var fade_time_start: float = self.duration_seconds - self.fade_seconds
	
	var to_fill: int = self._playback.get_frames_available()
	while (to_fill > 0):
		var sample = Sound.oscillator_sine(self._phase)
		
#		print_debug(self._seconds_since_start, is_before_attack_phase_end, is_before_fade_stage_start, is_before_end)
		
#		if self._seconds_since_start > self.duration_seconds + 1:
#			queue_free()
		
		amplitude = 1.0 # simple_envelope(self._seconds_since_start, self.duration_seconds, self.attack_seconds, self.fade_seconds)
		
		self._playback.push_frame(Vector2.ONE * amplitude * sample) # frames are stereo
		
		self._seconds_since_start += 1.0 / self.samples_per_second
		var phase_error = 0.01 * wave_per_sample * (randf() - 0.5)
		self._phase = fmod((self._phase + (wave_per_sample + phase_error)), 1.0)
		to_fill -= 1;


func simple_envelope(
	time_since_start_seconds: float,
	total_duration_seconds: float,
	attack_seconds: float,
	fade_seconds: float
) -> float:
		if time_since_start_seconds < 0.0:
			return 0.0
		elif time_since_start_seconds < attack_seconds:
			return time_since_start_seconds / attack_seconds
		elif time_since_start_seconds < total_duration_seconds - fade_seconds:
			return 1.0
		elif time_since_start_seconds < total_duration_seconds:
			return 1 - (time_since_start_seconds - (total_duration_seconds - fade_seconds)) / fade_seconds
		elif time_since_start_seconds >= total_duration_seconds:
			return 0.0
		
		return 0.0


func _process(delta):
	self._fill_buffer()
	self._time_since_start_seconds += delta + (0.01 * delta) * (randf() - 0.5)
