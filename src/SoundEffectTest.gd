extends Node


onready var total_process_calls: int = 0
onready var sound_effect_scene: PackedScene = preload("res://src/SoundEffect.tscn")


func start_sound(note: float, octave: float) -> void:
	var a: SoundEffect = sound_effect_scene.instance()
	var hertz: float = Sound.note_octave_to_hertz(note, octave)
	a.signal_waves_per_second = hertz
	a.play()
	add_child(a)


func _ready() -> void:
	var Note = Sound.Note
	print_debug(Sound.note_octave_to_hertz(Note.A, 4))
	print_debug(Sound.note_octave_to_hertz(Note.C, 4))
	print_debug(Sound.note_octave_to_hertz(Note.E, 4))
#	get_tree().quit()
	start_sound(Note.C, 4)
	start_sound(Note.E, 4)
	start_sound(Note.G, 4)

#func _process(delta: float) -> void:
#	if total_process_calls % 20 == 0:
#		var a = sound_effect_scene.instance()
#		var note_index = randi() % 7
#		a.name = note_names[note_index]
#		add_child(a)
#		a.signal_waves_per_second = notes[note_index]
#		a.play()
#		print_debug(a.signal_waves_per_second)
#
#	total_process_calls += 1
